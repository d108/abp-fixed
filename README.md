Adblock Plus for iOS
====================

A content blocker extension for Safari for iOS.

Building
--------

### Requirements

- [Xcode 9 or later](https://developer.apple.com/xcode/)
- [Carthage](https://github.com/Carthage/Carthage)
- [Sourcery](https://github.com/krzysztofzablocki/Sourcery)
- [SwiftLint](https://github.com/realm/SwiftLint/) (optional)

### Building in Xcode

1. Copy the file `ABP-Secret-API-Env-Vars.sh` (available internally) into the same directory as `AdblockPlusSafari.xcodeproj`.
2. Run `carthage update` to install additional Swift dependencies.
3. Open _AdblockPlusSafari.xcodeproj_ in Xcode.
4. Build and run the project locally in Xcode _or_ run `build.py` to export a build for distribution.

### Repository Branch Changes (2017-Dec-27)

Objective-C development on the previous `master` branch has been deprecated and
relocated to the `objective-c` branch. All development has been merged into the
`swift` branch and this has been made the new `master` branch.

### Changing Xcode configurations

* Target **AdblockPlusSafari**
    - General > Bundle Identifier = (org.adblockplus.AdblockPlusSafari ||
org.adblockplus.devbuilds.AdblockPlusSafari)
    - General > Team = (Company || Enterprise)
    - Capabilities > App Groups = (group.org.adblockplus.AdblockPlusSafari ||
group.org.adblockplus.devbuilds.AdblockPlusSafari)
    - Edit scheme > Build Configuration = (Debug || Devbuild Debug)
* Target **AdblockPlusSafariActionExtension**
    - General > Bundle Identifier =
    (org.adblockplus.AdblockPlusSafari.AdblockPlusSafariActionExtension ||
    org.adblockplus.devbuilds.AdblockPlusSafari.AdblockPlusSafariActionExtension)
    - General > Team = (Company || Enterprise)
* Target **AdblockPlusSafariExtension**
    - General > Bundle Identifier = (org.adblockplus.AdblockPlusSafari.AdblockPlusSafariExtension
    || org.adblockplus.devbuilds.AdblockPlusSafari.AdblockPlusSafariExtension)
    - General > Team = (Company || Enterprise)

Xcode may need to be restarted before the changes will take effect. When the changes are
complete, the app should be able to run on a simulator or a device.
